
const getScroll = () => {

    const getScrollBarWidth = () => window.innerWidth - document.documentElement.getBoundingClientRect().width
    const cssScrollBarWidth = () => document.documentElement.style.setProperty('--scrollbar', `${getScrollBarWidth()}px`)

    addEventListener('load', cssScrollBarWidth)
    addEventListener('resize', cssScrollBarWidth)

}

export default getScroll