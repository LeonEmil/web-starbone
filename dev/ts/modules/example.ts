// Este es un ejemplo de como exportar funciones desde un archivo
// En index.js se importan estas funciones

export const saludo = ():void => {
    console.log('Hola mundo')
};

export const despedida = ():void => {
    console.log('Adiós mundo')
};